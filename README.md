# Flask-SQLAlchemy-Alembic-template

Template app used Flask, SQLAlchemy, Alembic, Docker, Nginx, Gunicorn
base code for start, test and etc.

## Getting started

For start you need clone project.

### Developer version

For start developer version you need run
```bash
docker-compose up -d --build
```

Check you app on http://localhost:5000/

### Production version

Before start prod version you need create two file

1. `.env.prod` - enviroment of you web service, like this
```
FLASK_APP=project/__init__.py
FLASK_ENV=production
DATABASE_URL=postgresql://hello_flask:hello_flask@db:5432/hello_flask_prod
SQL_HOST=db
SQL_PORT=5432
DATABASE=postgres
APP_FOLDER=/home/app/web
```

2. `.env.prod.db` - enviroment of you database, like this
```
POSTGRES_USER=hello_flask
POSTGRES_PASSWORD=hello_flask
POSTGRES_DB=hello_flask_prod
```

Just now, for start prod version you need run
```bash
docker-compose -f docker-compose.prod.yml up -d --build
```

Check you app on http://localhost:1337/

### Migrations

Migration without docker
```bash
(env) user@host:~/path_to_project/flask-sqlalchemy-alembic-template$ export FLASK_APP=./services/web/manage.py 
(env) user@host:~/path_to_project/flask-sqlalchemy-alembic-template$ flask db init
(env) user@host:~/path_to_project/flask-sqlalchemy-alembic-template$ flask db migrate
```
