from flask.cli import FlaskGroup
from flask_migrate import Migrate

from project import app, db
from project.models import User


cli = FlaskGroup(app)
migrate = Migrate(app, db)


@cli.command("create_db")
def create_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command("seed_db")
def seed_db():
    db.session.add(User(email="grisha.fioktistov@yandex.ru"))
    db.session.commit()


if __name__ == "__main__":
    cli()
