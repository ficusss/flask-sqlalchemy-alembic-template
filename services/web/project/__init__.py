import os
from pydantic import BaseSettings, SecretStr

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_mail import Mail


class MailSettings(BaseSettings):
    MAIL_SERVER: str
    MAIL_PORT: int
    MAIL_USE_TLS: bool
    MAIL_USE_SSL: bool
    MAIL_DEFAULT_SENDER: str
    MAIL_USERNAME: str
    MAIL_PASSWORD: str


app = Flask(__name__)
env_config = os.getenv("APP_SETTINGS", "config.DevelopmentConfig")
app.config.from_object(env_config)
app.config.update(MailSettings())  # type: ignore


db = SQLAlchemy(app)


login_manager = LoginManager()
login_manager.login_view = "user_login.login_get"
login_manager.init_app(app)

mail_manager = Mail(app)

from .models import User  # noqa: E402


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


from project.user_profile import user_profile_blueprint  # noqa: E402
app.register_blueprint(user_profile_blueprint)

from project.user_login import user_login_blueprint  # noqa: E402
app.register_blueprint(user_login_blueprint)

from project.views import main as main_blueprint  # noqa: E402
app.register_blueprint(main_blueprint)

# from project import views  # noqa: E402
