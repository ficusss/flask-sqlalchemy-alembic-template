from flask_login import UserMixin
from project import db


class User(UserMixin, db.Model):  # type: ignore
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(128), unique=True, nullable=False)
    active = db.Column(db.Boolean(), default=True, nullable=False)

    def __init__(self, email):
        self.email = email

    def __repr__(self):
        return f"<{self.id} | {self.email} | {self.active}>"
