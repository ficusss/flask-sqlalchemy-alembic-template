import email
import json
from flask import (
    Blueprint,
    render_template,
    request,
    flash,
    redirect,
    url_for,
)
from flask_login import login_user, login_required, logout_user
from pydantic import BaseModel, EmailStr, ValidationError, validator
from flask_pydantic import validate

from .controllers import get_user, send_code, check_code
from .controllers import CheckCode, MAX_LOGIN_CODE_TRY


user_login = Blueprint('user_login', __name__)


class LoginRequestModel(BaseModel):
    email: EmailStr
    code: str | None
    remember: bool | None

    @validator("email")
    def to_email_str(cls, email: str):
        return EmailStr(email)

    @validator("code")
    def empty_str(cls, code: str):
        return code if len(code) else "_"

    @validator("remember")
    def to_bool(cls, remember: str):
        return True if remember else False


@user_login.route('/login', methods=["GET"])
def login_get():
    return render_template("user_login/login_email.html", data=None)


@user_login.route('/login', methods=["POST"])
@validate()
def login_post():

    try:
        data = LoginRequestModel(**request.form)  # type: ignore
    except ValidationError as e:
        print(e)
        template = "user_login/login_code.html" if request.form.get("email") \
            else "user_login/login_email.html"
        return render_template(
            template,
            data=None,
            errors=json.loads(e.json())
        )

    if data.code:
        code_info: CheckCode = check_code(data.email, data.code)
        code_try_left = MAX_LOGIN_CODE_TRY - code_info.try_count
        if code_info.is_valid:
            user = get_user(data.email)
            login_user(user, remember=data.remember)
            return redirect(url_for('user_profile.profile'))
        elif code_try_left > 0:
            flash(f"Неверный код. Осталось  попыток: {code_try_left}.")
            return render_template("user_login/login_code.html", data=data)
        else:
            flash("Неверный код. Проверьте Email и попробуйте снова.")
            return render_template("user_login/login_email.html", data=data)
    else:
        send_code(data.email)
        return render_template("user_login/login_code.html", data=data)


@user_login.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))
