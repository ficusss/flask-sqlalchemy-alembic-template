from dataclasses import dataclass
from random import randint
from project import db, mail_manager
from project.models import User
from project.user_login.models import LoginCode
from flask_mail import Message


MAX_LOGIN_CODE_TRY = 3


@dataclass
class CheckCode:
    is_valid: bool
    try_count: int = 0


def send_code(email: str):
    code = str(randint(0, 999999)).rjust(6, '0')

    login_code = LoginCode(email=email, code=code)
    db.session.add(login_code)
    db.session.commit()

    msg = Message("You code", recipients=[email])
    msg.body = f"You code: {code}"
    mail_manager.send(msg)


def check_code(email: str, code: str):
    login_code = LoginCode.query.filter_by(email=email) \
        .order_by(LoginCode.create_datatime).all()[-1]
    login_code.try_count += 1
    db.session.commit()
    
    if code == login_code.code:
        return CheckCode(is_valid=True)
    
    return CheckCode(
        is_valid=False, 
        try_count=login_code.try_count
    )


def get_user(email):
    user = User.query.filter_by(email=email).first()
    if not user:
        user = User(email=email)
        db.session.add(user)
        db.session.commit()
    return user
