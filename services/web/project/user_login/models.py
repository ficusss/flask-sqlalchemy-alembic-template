import datetime
from project import db


class LoginCode(db.Model):  # type: ignore
    __tablename__ = "login_code"

    id = db.Column(db.Integer(), primary_key=True)
    email = db.Column(db.String(128), nullable=False)
    code = db.Column(db.String(6), nullable=False)
    try_count = db.Column(db.Integer(), default=0)
    create_datatime = db.Column(db.DateTime(), default=datetime.datetime.now())
